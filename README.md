# FDO Manager (API)

## Tables of contents
* [Project description](#project-description)
* [Features](#features)
* [API endpoints](#api-endpoints)
* [Running the FDO Manager](#running-the-fdo-manager)
* [Contributing](#contributing)
* [People and Organizations](#people-and-organizations)
* [Contact Us](#contact-us)

### Project description
The Fdo manager allows the management of Artifcats following the standards of Schema.org. 

### Features
These are the already implemented features of the Fdo manager
* CRUD operations ()
  * Inserting Artifiacts such as (code, dataset, article, person, organization...)
  * Modifying existing artifacts.
  * Deleting existing artifacts.
  * Patching existing artifacts.
* .

### API endpoints
List all the available API endpoints and their corresponding HTTP methods. Include a brief description of what each endpoint does. For example:

* `Person`
  * GET: `http://127.0.0.1:8000/getPerson/`
  * POST: `http://127.0.0.1:8000/addPerson/`
  * PUT: `http://127.0.0.1:8000/persons/{id}/`
  * PATCH: `http://127.0.0.1:8000/persons/{id}/patch/`
  * DELETE: `http://127.0.0.1:8000/persons/{id}/delete/`
* `Organization` 
  * GET: `http://127.0.0.1:8000/getOrganization/`
  * POST: `http://127.0.0.1:8000/addOrganization/`
  * PUT: `http://127.0.0.1:8000/organization/{id}/`
  * PATCH: `http://127.0.0.1:8000/organization/{id}/patch/`
  * DELETE: `http://127.0.0.1:8000/organization/{id}/delete/`
* `CreativeWork`
  * GET: `http://127.0.0.1:8000/getCreativeWork/`
  * POST: `http://127.0.0.1:8000/addCreativeWork/`
  * PUT: `http://127.0.0.1:8000/creativework/{id}/`
  * PATCH: `http://127.0.0.1:8000/creativework/{id}/patch/`
  * DELETE: `http://127.0.0.1:8000/creativework/{id}/delete/`
* `Service`
  * GET: `http://127.0.0.1:8000/getService/`
  * POST: `http://127.0.0.1:8000/addService/`
  * PUT: `http://127.0.0.1:8000/service/{id}/`
  * PATCH: `http://127.0.0.1:8000/service/{id}/patch/`
  * DELETE: `http://127.0.0.1:8000/service/{id}/delete/`
* `WebAPI`
  * GET: `http://127.0.0.1:8000/getWebapi/`
  * POST: `http://127.0.0.1:8000/addWebapi/`
  * PUT: `http://127.0.0.1:8000/webapi/{id}/`
  * PATCH: `http://127.0.0.1:8000/webapi/{id}/patch/`
  * DELETE: `http://127.0.0.1:8000/webapi/{id}/delete/`
* `Software application`
  * GET: `http://127.0.0.1:8000/getSoftwareApp/`
  * POST: `http://127.0.0.1:8000/addSoftwareApp/`
  * PUT: `http://127.0.0.1:8000/softwareApp/{id}/`
  * PATCH: `http://127.0.0.1:8000/softwareApp/{id}/patch/`
  * DELETE: `http://127.0.0.1:8000/softwareApp/{id}/delete/`


## Running the FDO Manager
### Running locally
To be able to run the FDO Manager locally, the following is required: 
* A recent version of Mysql needs to be installed on the system.
* Create an empty Schema or DB in mysql called "fdodb".
* A recent version of Python.
* Clone the project  `https://gitlab.com/fdda1/fdo-manager.git`
* Create a virtual environment and install the required packages in `requirements.txt`.
* Run the migrations commands:
  * `python manage.py makemigrations` 
  * `python manage.py migrate`.
* Run the FDO Manager with `python manage.py runserver`.


### Running with docker
to be able to run with docker, a latest version of docker must be installed.
* Navigate to the folder in which the docker compose file exists (FDO api)
* Start the container with: 
  * `docker-compose -f docker-compose-dev.yml up --build`

There are two docker-compose files (`docker-compose-dev.yml` and `docker-compose-prod.yml`) meant for development and production respectively. Please change the configuration in the env variables as required.

## Contributing
In order to contribute, please: 
- Clone the project locally `https://gitlab.com/fdda1/fdo-manager.git`
- Create a new branch `git checkout -b branchname`
- Make change and push the code
- Create merge request

## People and Organizations 
This project is maintained and contributed to by:
- Oussama Zoubia (Uni Klinik Köln) 
- Zeyd boukhers (Fraunhofer FIT, Uni Klinik Köln)
- Nagaraj Bahubali Asundi (Fraunhofer FIT)
- Oya Beyan (Fraunhofer FIT, Uni Klinik Köln)
- Adamantios Koumpis (Uni Klinik Köln)


## Contact Us
For any inquiries or feedback, please don't hesitate to [contact us](mailto:zoubia.oussama@uk-koeln.de) 
