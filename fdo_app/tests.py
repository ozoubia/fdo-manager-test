from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from .models import Organization


class MyModelTests(TestCase):
    def setUp(self):
        # Create test data
        self.my_model = Organization.objects.create(name='Test Name', url='test url', identifier='test id',
                                                    description='test description', legalName='test legal name',
                                                    address='test address', email='test email')

    def test_model_creation(self):
        # Test that the model is created correctly in the setUp method
        self.assertEqual(Organization.objects.count(), 1)
        self.assertEqual(self.my_model.name, 'Test Name')
