## General Issue Template

### Description
[//]: # (Provide a brief description of the issue.)

### Motivation
[//]: # (Explain the motivation behind this issue or why it is needed.)

### Context
[//]: # (Provide any additional context or background information.)

### Steps to Reproduce (if applicable)
1. [//]: # (Step 1)
2. [//]: # (Step 2)
3. [//]: # (Step 3)

### Expected Behavior (if applicable)
[//]: # (Describe what you expected to happen.)

### Actual Behavior (if applicable)
[//]: # (Describe what actually happened.)

### Screenshots or Additional Information (if applicable)
[//]: # (Include screenshots or additional information that may be helpful.)

### Proposed Solution or Ideas
[//]: # (Outline any proposed solutions or ideas for addressing the issue.)

### Acceptance Criteria
[//]: # (Define the conditions that must be met for this issue to be considered resolved.)

### Dependencies
[//]: # (List any dependencies or related issues that need to be addressed.)

### Additional Notes
[//]: # (Any additional information or context that may be relevant.)
